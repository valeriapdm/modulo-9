import requests

#obtiene todos loas datos de una api
def getAll(url):
    next = url
    data = []
    while next:
        res = requests.get(next)
        res = res.json()
        next = res['next']
        for item in res['results']:
            data.append(item)
    return data

#cuales peliculas tienen planetas aridos
def peliculasDondeHayPlanetasAridos(clima = 'arid'):
    peliculas = getAll('https://swapi.dev/api/films/')
    data = []
    for film in peliculas:
        for j in film['planets']:
            planet = requests.get(j).json()
            if planet['climate'] == clima:
                data.append(film['title'])
                break    
    return data

#Obtener la nave mas grande
#obtiene todos los datos y lugo compara
def naveMasGrande(url = 'https://swapi.dev/api/starships/'):
    naves = getAll(url)
    data = naves[0]
    for item in naves:
        if float(item['length'].replace(',', '')) > float(data['length'].replace(',', '')):
            data = item
    return data


#Calcula cuantos wookie aparecen en la sexta pelicula de starwar 
#primero obtine el url de cada wookie 
#luego ve en el resultado de la peticion get de la sexta pelicula posee algunos de estos link en su apartado de personajes
def wookieSextaPelicula():
    wookieInFilm = []
    wookie = requests.get('https://swapi.dev/api/species/?search=wookie').json()
    wookie = wookie['results'][0]['people']
    peopleOfFilm = requests.get('https://swapi.dev/api/films/6/').json()
    peopleOfFilm = peopleOfFilm['characters']
    for item in peopleOfFilm:
        for j in wookie:
            if item == j:
                wookieInFilm.append(j)
    return wookieInFilm

if __name__ == '__main__':
    pelicula = peliculasDondeHayPlanetasAridos()
    nave = naveMasGrande()
    wookieEnPelicula = wookieSextaPelicula()
    print('¿En cuantas peliculas aparecen planetas cuyo clima sea arido?')
    print(len(pelicula))
    print('¿cuantos wookies aparecen en la sexta pelicula?')
    print(len(wookieEnPelicula))
    print('¿cual es el nombre de la aeronave mas grande en toda la saga?')
    print(nave['name'])

